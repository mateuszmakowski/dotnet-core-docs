﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace aspnetcoreappmvc.Models
{
    public class aspnetcoreappmvcContext : DbContext
    {
        public aspnetcoreappmvcContext(DbContextOptions<aspnetcoreappmvcContext> options)
            : base(options)
        {
        }

        public DbSet<aspnetcoreappmvc.Models.Movie> Movie { get; set; }
    }
}
